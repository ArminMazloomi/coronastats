// server.js
// load the things we need
require('dotenv').config();

const express = require('express');
const axios = require('axios');
const home = require('./backend/home');

const port = process.env.PORT || 3000;
var app = express();

app.set('view engine', 'ejs');
app.get('/', function(req, res) {
    res.render('pages/index');
});
app.use('/home', home);
app.use(function(req, res) {
	return res.status(404).json({message: "Not Found"});
});

app.listen(port);
